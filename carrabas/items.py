# -*- coding: utf-8 -*-

import scrapy
from scrapy.item import Item, Field

class CarrabasItem(scrapy.Item):
    address_first = Field()
    address = Field()
    city = Field() 
    state = Field()
    zip_code = Field()
    phone = Field()
    store_name = Field()
    store_id = Field()
    store_hours = Field()
