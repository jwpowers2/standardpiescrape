import scrapy
import time
from random import randint
import json
from scrapy.http import FormRequest
from carrabas.items import CarrabasItem


class CarrabasSpider(scrapy.Spider):

    name = "carrabas"
    allowed_domains = ["carrabbas.com"]

    def start_requests(self):
        states = ["ALABAMA", "ALASKA", "ARIZONA", "ARKANSAS",
                    "CALIFORNIA", "COLORADO", "CONNECTICUT", 
                    "DELAWARE", "FLORIDA", "GEORGIA", "HAWAII",
                    "IDAHO", "ILLINOIS", "INDIANA", "IOWA",
                    "KANSAS", "KENTUCKY", "LOUISIANA", "MAINE",
                    "MARYLAND", "MASSACHUSETTS", "MICHIGAN",
                    "MINNESOTA", "MISSISSIPPI", "MISSOURI",
                    "MONTANA", "NEBRASKA", "NEVADA", "NEW HAMPSHIRE",
                    "NEW JERSEY", "NEW MEXICO", "NEW YORK", 
                    "NORTH CAROLINA", "NORTH DAKOTA", "OHIO",
                    "OKLAHOMA", "OREGON", "PENNSYLVANIA", "RHODE ISLAND",
                    "SOUTH CAROLINA", "SOUTH DAKOTA", "TENNESSEE",
                    "TEXAS", "UTAH", "VERMONT", "VIRGINIA",
                    "WASHINGTON", "WEST VIRGINIA", "WISCONSIN", "WYOMING" ]

        for state in states:
            time.sleep(randint(1,4))
            frmdata={'state':state}
            yield FormRequest(url="https://www.carrabbas.com/mvc/site/GetStoresByState/?", formdata=frmdata, method='GET')


    def parse(self, response):
        
        address_first = response.xpath('//html').extract()
        address_first = address_first.pop(0)                
        address_first = address_first.rstrip("</p></body></html>")
        address_first = address_first.lstrip("<html><body><p>")
        address_first = json.loads(address_first)
        
        for i in address_first:

            item = CarrabasItem()
            hours_one = i.get("Hours")
            item['address'] = i.get("Address")
            item['city'] = i.get("City")
            item['state'] = i.get("State")
            item['zip_code'] = i.get("Zip")
            item['phone'] = i.get("Phone")
            item['store_name'] = i.get("Name")
            item['store_id'] = i.get("Id")
            item['store_hours'] = "Mon: {}-{}, Tue: {}-{}, Wed: {}-{}, Thu: {}-{}, Fri: {}-{}, Sat: {}-{}, Sun: {}-{}".format(hours_one.get("MondayOpen"), hours_one.get("MondayClose"), 
                                                                                                                              hours_one.get("TuesdayOpen"), hours_one.get("TuesdayClose"),
                                                                                                                              hours_one.get("WednesdayOpen"), hours_one.get("WednesdayClose"),
                                                                                                                              hours_one.get("ThursdayOpen"), hours_one.get("ThursdayClose"),
                                                                                                                              hours_one.get("FridayOpen"), hours_one.get("FridayClose"),
                                                                                                                              hours_one.get("SaturdayOpen"), hours_one.get("SaturdayClose"),
                                                                                                                              hours_one.get("SundayOpen"), hours_one.get("SundayClose"))

            yield item

