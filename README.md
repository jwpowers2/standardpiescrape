
# Standard type of iterative scrape using form parameters 

## python scrapy framework 

## How to set up scrapy and use it

# NOTE: I built in a delay because scrapy is really fast

## set up this scrape and run it

## Ubuntu 16

    apt-get update

    apt-get install python-pip python-dev libffi-dev libssl-dev libxml2-dev libxslt1-dev 

    pip install scrapy

    git clone https://bitbucket.org/jwpowers2/standardpiescrape

    cd standardpiescrape

## crawl page and send to stdout

    scrapy crawl carrabas

## crawl and send to stdout and to output json file

    -o output.json -t json

## crawl and send to stdout and to output csv file

    -o output.csv -t csv


